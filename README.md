# Laravel-CampaignMonitor

[![Total Downloads](https://poser.pugx.org/bashy/laravel-campaignmonitor/downloads?format=flat-square)](https://packagist.org/packages/bashy/laravel-campaignmonitor)

A Laravel 5 wrapper for CampaignMonitor APIs

- [Laravel-CampaignMonitor on Packagist](https://packagist.org/packages/bashy/laravel-campaignmonitor)
- [Laravel-CampaignMonitor on GitHub](https://github.com/bbashy/Laravel-CampaignMonitor)


## Installation

Pull in the package through Composer;
```
composer require bashy/laravel-campaignmonitor
```

If you're using Laravel >= 5.5, please skip this as this package will be auto-discovered.
Add the service provider to `config/app.php`
```php
Bashy\CampaignMonitor\CampaignMonitorServiceProvider::class,
```

This package has a Laravel facade. You can register it in the `aliases` array in the `config/app.php` file
```php
'CampaignMonitor' => Bashy\CampaignMonitor\Facades\CampaignMonitor::class,
```

Publish the config file
```
$ php artisan vendor:publish --provider="Bashy\CampaignMonitor\CampaignMonitorServiceProvider"
```

And set your own API key and Client ID via .env or similar to match these
```
CAMPAIGNMONITOR_API_KEY=YourKey
CAMPAIGNMONITOR_CLIENT_ID=123456789
```

## Usage

You can find all the methods in the original [campaignmonitor/createsend-php package](https://github.com/campaignmonitor/createsend-php).

Some examples;
```php
// Add a subscriber to a list
$result = CampaignMonitor::subscribers('LIST_ID')->add([
    'EmailAddress' => 'email@example.com',
    'Name' => 'Ben',
]);
```
```php
// Create a list for your client
$result = CampaignMonitor::lists()->create(config('campaignmonitor.client_id'), [
    'Title' => 'List name',
]);
```

To send classic transactional emails
```php
$data = [
    'From' => 'from@example.org',
    'To' => 'to@example.org',
    'ReplyTo' => 'replyto@example.org',
    'CC' => 'cc@example.org',
    'BCC' => 'bcc@example.org',
    'HTML' => '<p>Hello there!</p>',
    'Text' => 'Hello there!',
];

CampaignMonitor::classicSend('CLIENT_ID')->send($data);
```
